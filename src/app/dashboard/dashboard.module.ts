import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardTableComponent } from './components/dashboard-table/dashboard-table.component';
import { SharedModule } from '../shared/shared.module';
import { DashboardFormComponent } from './components/dashboard-form/dashboard-form.component';
import { NextPaymentDetailsComponent } from './components/next-payment-details/next-payment-details.component';
import { BalanceDetailsComponent } from './components/balance-details/balance-details.component';

@NgModule({
  declarations: [DashboardComponent, DashboardTableComponent, DashboardFormComponent, NextPaymentDetailsComponent, BalanceDetailsComponent],
  imports: [DashboardRoutingModule, SharedModule],
})
export class DashboardModule {}
