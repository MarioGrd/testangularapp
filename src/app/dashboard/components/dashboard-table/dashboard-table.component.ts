import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

export interface PaymentHistory {
  date: string;
  transactionIdentifier: string;
  transactionDescription: string;
  effectiveDate: string;
  paidToDate: string;
  amountPosted: number;
  principalAmount: number;
  principalBalanceAmount: number;
  interestsAmount: number;
  escrowAmount: number;
  lateChangeAmount: number;
  unappliedFundsAmount: number;
  unapliedFundsBalanceAfterTransaction: number;
}

const DATA: PaymentHistory[] = [
  {
    date: '01/01/2020',
    transactionIdentifier: 'TI',
    transactionDescription: 'TA',
    effectiveDate: '01/01/2020',
    paidToDate: '01/01/2020',
    amountPosted: 1,
    principalAmount: 1000,
    principalBalanceAmount: 200,
    interestsAmount: 100,
    escrowAmount: 100,
    lateChangeAmount: 2,
    unappliedFundsAmount: 2,
    unapliedFundsBalanceAfterTransaction: 3,
  },
  {
    date: '01/01/2020',
    transactionIdentifier: 'TI',
    transactionDescription: 'TA',
    effectiveDate: '01/01/2020',
    paidToDate: '01/01/2020',
    amountPosted: 1,
    principalAmount: 1000,
    principalBalanceAmount: 200,
    interestsAmount: 100,
    escrowAmount: 100,
    lateChangeAmount: 2,
    unappliedFundsAmount: 2,
    unapliedFundsBalanceAfterTransaction: 3,
  },
];

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.scss'],
})
export class DashboardTableComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  //displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  columns: string[] = [
    'date',
    'transactionIdentifier',
    'transactionDescription',
    'effectiveDate',
    'paidToDate',
    'amountPosted',
    'principalAmount',
    'principalBalanceAmount',
    'interestsAmount',
    'escrowAmount',
    'lateChangeAmount',
    'unappliedFundsAmount',
    'unapliedFundsBalanceAfterTransaction',
  ];

  displayedColumns: string[];
  dataSource: MatTableDataSource<PaymentHistory>;

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
    this.displayedColumns = this.columns;

    this.breakpointObserver.observe(Breakpoints.Handset).subscribe((result) => {
      this.displayedColumns = result.matches
        ? this.columns.filter((item, index) => index < 3)
        : this.columns;
    });

    this.breakpointObserver.observe(Breakpoints.Medium).subscribe((result) => {
      this.displayedColumns = result.matches
        ? this.columns.filter((item, index) => index < 7)
        : this.columns;
    });

    this.dataSource = new MatTableDataSource(DATA);
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }
}
