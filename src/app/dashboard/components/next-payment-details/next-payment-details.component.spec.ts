import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NextPaymentDetailsComponent } from './next-payment-details.component';

describe('NextPaymentDetailsComponent', () => {
  let component: NextPaymentDetailsComponent;
  let fixture: ComponentFixture<NextPaymentDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NextPaymentDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NextPaymentDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
