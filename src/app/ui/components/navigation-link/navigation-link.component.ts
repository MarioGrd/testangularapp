import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

import { RouteInfo } from '../../models/navigation.interface';

@Component({
  selector: 'app-navigation-link',
  templateUrl: './navigation-link.component.html',
  styleUrls: ['./navigation-link.component.scss'],
  animations: [
    trigger('animationShowHide', [
      state('close', style({ height: '0px', overflow: 'hidden' })),
      state('open', style({ height: '*', overflow: 'hidden' })),
      transition('open <=> close', animate('300ms ease-in-out')),
    ]),
    trigger('animationRotate', [
      state('close', style({ transform: 'rotate(0)' })),
      state('open', style({ transform: 'rotate(180deg)' })),
      transition('open <=> close', animate('300ms ease-in-out')),
    ]),
  ],
})
export class NavigationLinkComponent {
  @Input() public menuItem: RouteInfo;
  @Output() public expand = new EventEmitter<RouteInfo>();

  public state = 'close';

  constructor() {}

  public onExpand(): void {
    if (this.menuItem.isCollapsed) {
      this.state = 'close';
    } else {
      this.state = 'open';
    }

    this.expand.emit(this.menuItem);
  }
}
