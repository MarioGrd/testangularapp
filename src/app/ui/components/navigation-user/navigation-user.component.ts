import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-navigation-user',
  templateUrl: './navigation-user.component.html',
  styleUrls: ['./navigation-user.component.scss'],
  animations: [
    trigger('animationShowHide', [
      state('close', style({ height: '0px', overflow: 'hidden' })),
      state('open', style({ height: '*', overflow: 'hidden' })),
      transition('open <=> close', animate('300ms ease-in-out')),
    ]),
    trigger('animationRotate', [
      state('close', style({ transform: 'rotate(0)' })),
      state('open', style({ transform: 'rotate(180deg)' })),
      transition('open <=> close', animate('300ms ease-in-out')),
    ]),
  ],
})
export class NavigationUserComponent {
  @Input() public isUserInfoOpened: boolean;
  @Output() public toggleUserInfo = new EventEmitter();

  public state = 'close';

  constructor() {}

  public onOpenUserInfo() {
    if (this.isUserInfoOpened) {
      this.state = 'close';
    } else {
      this.state = 'open';
    }

    this.toggleUserInfo.emit();
  }
}
