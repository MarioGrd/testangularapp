import { NavigationLinkComponent } from './navigation-link/navigation-link.component';
import { NavigationLoanComponent } from './navigation-loan/navigation-loan.component';
import { NavigationLogoComponent } from './navigation-logo/navigation-logo.component';
import { NavigationUserComponent } from './navigation-user/navigation-user.component';

export const components = [
  NavigationLinkComponent,
  NavigationLogoComponent,
  NavigationUserComponent,
  NavigationLoanComponent,
];
