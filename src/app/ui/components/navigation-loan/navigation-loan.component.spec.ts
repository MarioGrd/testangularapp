import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationLoanComponent } from './navigation-loan.component';

describe('NavigationLoanComponent', () => {
  let component: NavigationLoanComponent;
  let fixture: ComponentFixture<NavigationLoanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigationLoanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
