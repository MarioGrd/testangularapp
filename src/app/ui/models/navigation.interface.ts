export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  isCollapsed?: boolean;
  collapse?: string;
  children?: RouteItem[];
}

export interface RouteItem {
  path: string;
  title: string;
  ab: string;
  type?: string;
}
