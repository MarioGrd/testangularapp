import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { components } from './components/index';
import { containers } from './containers/index';
import { NavigationLoanComponent } from './components/navigation-loan/navigation-loan.component';

@NgModule({
  exports: [...containers],
  declarations: [...components, ...containers, NavigationLoanComponent],
  imports: [SharedModule, RouterModule],
})
export class UiModule {}
