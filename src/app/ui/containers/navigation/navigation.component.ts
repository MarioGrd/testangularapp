import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';

import { map, shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { RouteInfo } from '../../models/navigation.interface';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  isUserInfoOpened = false;

  public menuItems: RouteInfo[] = [
    {
      path: '/dashboard',
      title: 'Dashboard',
      type: 'link',
      icontype: 'dashboard',
    },
    {
      path: '/account',
      title: 'Account',
      type: 'sub',
      icontype: 'settings',
      isCollapsed: true,
      children: [
        {
          path: 'alerts',
          title: 'Alerts',
          ab: 'A',
          type: 'link',
        },
        {
          path: 'preferences',
          title: 'Preferences',
          ab: 'P',
          type: 'link',
        },
      ],
    },
    {
      path: '/documents',
      title: 'Documents',
      type: 'link',
      icontype: 'description',
    },
    {
      path: 'faq',
      title: 'FAQ',
      type: 'link',
      icontype: 'help_outline',
    },
  ];

  constructor(private breakpointObserver: BreakpointObserver) {}

  public onOpenUserInfo() {
    this.isUserInfoOpened = !this.isUserInfoOpened;
  }

  public onExpand(item: RouteInfo) {
    const isCollapsed = this.menuItems.find((mi) => mi.title === item.title)
      .isCollapsed;

    if (isCollapsed === null || isCollapsed === undefined) {
      return;
    }

    this.menuItems.find(
      (mi) => mi.title === item.title
    ).isCollapsed = !isCollapsed;
  }
}
