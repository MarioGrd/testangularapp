import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FaqComponent } from './faq.component';
import { FaqRoutingModule } from './faq-routing.module';

@NgModule({
  declarations: [FaqComponent],
  imports: [CommonModule, FaqRoutingModule],
})
export class FaqModule {}
