import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AddLoanComponent } from './components/add-loan/add-loan.component';
import { AlertsComponent } from './containers/alerts/alerts.component';
import { PreferencesComponent } from './containers/preferences/preferences.component';

const routes: Routes = [
  { path: 'alerts', component: AlertsComponent },
  { path: 'preferences', component: PreferencesComponent },
  { path: 'add', component: AddLoanComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
