import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AccountComponent } from './account.component';

import { AccountRoutingModule } from './account-routing.module';

import { components } from './components/index';
import { containers } from './containers/index';

import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [...containers, ...components, AccountComponent],
  imports: [CommonModule, AccountRoutingModule, SharedModule],
})
export class AccountModule {}
