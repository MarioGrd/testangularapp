import { AlertsComponent } from './alerts/alerts.component';
import { PreferencesComponent } from './preferences/preferences.component';

export const containers = [AlertsComponent, PreferencesComponent];
